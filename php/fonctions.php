<?php
require_once("../sql/INC.php");

function addVideo($title, $description)
{
    $Message = "";
    $Error = 0;

    if ($_FILES["Video"]["name"] != "") {
        //Check if there is a video file in $_file and with using the index 0 we are sure to only care about the first file 
        $array = explode('.', $_FILES['Video']['name']);
        $extension = end($array);
        //we check if the file is in the 
        if ($extension == "mp4" || $extension == "MP4") {
            $Message .= "
            <div class='uk-alert-success uk-border-rounded' uk-alert style='background-color:#7be4a5;color:black'>
            <a class='uk-alert-close' uk-close></a>
            <p>The video is in a good format and will be uploaded soon</p>
            </div>";



            $tmp_name = $_FILES["Video"]["tmp_name"];
            $path = $_FILES["Video"]["name"];
            $newName = uniqid() . "." . pathinfo($path, PATHINFO_EXTENSION);
            $videoServerPath = "../videos/" . $newName;

            if (move_uploaded_file($tmp_name, $videoServerPath)) {
                $Message .= "
                <div class='uk-alert-success uk-border-rounded' uk-alert style='background-color:#7be4a5;color:black'>
                <a class='uk-alert-close' uk-close></a>
                <p>The video Has been successfully uploaded to the server</p>
                </div>";
                //if the file is upload to the server successfully you can upload data to db

                $DB = connexion();

                try {
                    connexion()->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
                    $poste = connexion()->prepare('INSERT INTO t_videos VALUES (null,1,0,NOW(),"' . $title . '","' . $description . '","' . $videoServerPath . '",1,0,0,0)');
                    $poste->execute();



                    if (file_exists($_FILES["Thumbnail"]["tmp_name"])) {

                        $array = explode('.', $_FILES['Thumbnail']['name']);
                        $extension = end($array);
                        //we check if the file is in a good format
                        if ($extension == "jpg" || $extension == "JPG" || $extension == "png" || $extension == "PNG") {

                            //if there is a thumbnail , we can upload it , else we dont do anything 
                            $tmp_name = $_FILES["Thumbnail"]["tmp_name"];
                            //var_dump(connexion()->lastInsertId());
                            //last insert id does'nt work for no apparent reason so im going to try something else
                            $lastInsert = connexion()->prepare('SELECT max(idVideo) FROM t_videos');
                            $lastInsert->execute();
                            $lastId = $lastInsert->fetch();

                            //var_dump($lastId[0]);
                            $path = $_FILES["Thumbnail"]["name"];
                            $newName = $lastId[0] . "." . pathinfo($path, PATHINFO_EXTENSION);
                            try {
                                //echo "../thumbnail/".$newName;
                                move_uploaded_file($tmp_name, "../thumbnail/" . $newName);
                            } catch (\Throwable $th) {
                                $Message .= "
                                <div class='uk-alert-danger uk-border-rounded' uk-alert style='background-color:#d64550;color:black'>
                                <a class='uk-alert-close' uk-close></a>
                                <p>Problem with the upload of the thumbnail</p>
                                </div>";
                            }
                        } else {
                            $Message .= "
                            <div class='uk-alert-danger uk-border-rounded' uk-alert style='background-color:#d64550;color:black'>
                            <a class='uk-alert-close' uk-close></a>
                            <p>Problem uploading the thumbnail , you can only upload jpg and png files here</p>
                            </div>";
                        }
                    }
                } catch (\Throwable $th) {
                    $Message .= "
                    <div class='uk-alert-danger uk-border-rounded' uk-alert style='background-color:#d64550;color:black'>
                    <a class='uk-alert-close' uk-close></a>
                    <p>Database server error</p>
                    </div>";
                    $Error = 1;
                }
                if ($Error == 0) {

                    header("Location:index.php");
                }
            } else {
                $Message .= "
                <div class='uk-alert-danger uk-border-rounded' uk-alert style='background-color:#d64550;color:black'>
                <a class='uk-alert-close' uk-close></a>
                <p>There has been an error occuring wile trying to upload the video on the server , try later or contact our support</p>
                </div>";
            }
        } else {
            $Message .= "
            <div class='uk-alert-danger uk-border-rounded' uk-alert style='background-color:#d64550;color:black'>
            <a class='uk-alert-close' uk-close></a>
            <p>There has been an error during uploading, remember , the only files that we accept are mp4</p>
            </div>";
        }



        return $Message;
    }
}
function GetLastVideos($Offset, $nbrByOffset)
{
    $response = array();
    $requette = connexion()->prepare('SELECT * FROM t_videos ORDER BY publishDate DESC LIMIT ' . $Offset . ', ' . $nbrByOffset . '');
    $requette->execute();
    $posts = $requette->fetchAll();
    foreach ($posts as $p) {
        $strResponse = $p["title"] . ";:" . $p["description"] . ";:" . $p["location"] . ";:" . $p["vues"] . ";:" . $p["publishDate"] . ";:" . $p["idVideo"];
        array_push($response, $strResponse);
    }
    return $response;
}
function GetMostViewedVideos($Offset, $nbrByOffset)
{
    $response = array();
    $requette = connexion()->prepare('SELECT * FROM t_videos ORDER BY vues DESC LIMIT ' . $Offset . ', ' . $nbrByOffset . '');
    $requette->execute();
    $posts = $requette->fetchAll();
    foreach ($posts as $p) {
        $strResponse = $p["title"] . ";:" . $p["description"] . ";:" . $p["location"] . ";:" . $p["vues"] . ";:" . $p["publishDate"] . ";:" . $p["idVideo"];
        array_push($response, $strResponse);
    }
    return $response;
}
function DisplayLatestVideos()
{
    $return = GetLastVideos(0, 5);

    $count = 4;
    foreach ($return as $r) {
        $count++;
        if (count($return) < 5) {
            if ($count % 5 == 0 && $count != 4) {
                echo "</div>";
                echo "<div class='uk-child-width-1-" . count($return) . "@m uk-text-center uk-margin-large-bottom'uk-grid>";
            }
        } else {
            if ($count % 5 == 0 && $count != 4) {
                echo "</div>";
                echo "<div class='uk-child-width-1-5@m uk-text-center uk-margin-large-bottom'uk-grid>";
            }
        }

        $infos = explode(";:", $r);
        //echo "<div class=''>";

        echo "<a href='#' style='text-decoration:none'>";

        echo "<div class='uk-card uk-card-default uk-card-hover uk-card-secondary uk-padding-bottom '>";
        echo "<div class='uk-card-media-top'>";
        if (file_exists("../thumbnail/" . $infos[5] . ".jpg")) {
            echo "<img width='1920' height='1080' src='../thumbnail/" . $infos[5] . ".jpg' alt='' style='object-fit: cover;>";
        } else {
            if (file_exists("../thumbnail/" . $infos[5] . ".png")) {
                echo "<img width='1920' height='1080' src='../thumbnail/" . $infos[5] . ".png' alt='' style='object-fit: cover;'>";
            } else {
                echo "<img width='1920' height='1080' src='../thumbnail/Missing.jpg' alt='' style='object-fit: cover;>";
            }
        }
        echo "</div>";
        echo "<div class='uk-card-body' >";
        echo "<h3 class='uk-card-title'>" . $infos[0] . "</h3>";
        echo "<p>" . $infos[4] . "</p>";
        echo "</div>";
        echo "</div>";
        echo "</a>";
    }
    echo "</div>";
}
function DisplayMostViewedVideos(){
    $return = GetMostViewedVideos(0,5);

    $count = 4;
    foreach ($return as $r) {
        $count++;
        if (count($return) < 5) {
            if ($count % 5 == 0 && $count != 4) {
                echo "</div>";
                echo "<div class='uk-child-width-1-" . count($return) . "@m uk-text-center uk-margin-large-bottom'uk-grid>";
            }
        } else {
            if ($count % 5 == 0 && $count != 4) {
                echo "</div>";
                echo "<div class='uk-child-width-1-5@m uk-text-center uk-margin-large-bottom'uk-grid>";
            }
        }

        $infos = explode(";:", $r);
        //echo "<div class=''>";

        echo "<a href='#' style='text-decoration:none'>";

        echo "<div class='uk-card uk-card-default uk-card-hover uk-card-secondary uk-padding-bottom '>";
        echo "<div class='uk-card-media-top'>";
        if (file_exists("../thumbnail/" . $infos[5] . ".jpg")) {
            echo "<img width='1920' height='1080' src='../thumbnail/" . $infos[5] . ".jpg' alt='' style='object-fit: cover;>";
        } else {
            if (file_exists("../thumbnail/" . $infos[5] . ".png")) {
                echo "<img width='1920' height='1080' src='../thumbnail/" . $infos[5] . ".png' alt='' style='object-fit: cover;'>";
            } else {
                echo "<img width='1920' height='1080' src='../thumbnail/Missing.jpg' alt='' style='object-fit: cover;>";
            }
        }
        echo "</div>";
        echo "<div class='uk-card-body' >";
        echo "<h3 class='uk-card-title'>" . $infos[0] . "</h3>";
        echo "<p>" . $infos[3] . " vues</p>";
        echo "</div>";
        echo "</div>";
        echo "</a>";
    }
    echo "</div>";
}
