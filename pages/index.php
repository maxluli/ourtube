<?php

require_once("../php/fonctions.php");
//phpinfo();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="../css/uikit.min.css" />
    <script src="../js/uikit.min.js"></script>
    <script src="../js/uikit-icons.min.js"></script>

    <title>Document</title>
</head>

<body>
    <?php include_once("../php/nav.php"); ?>

    
    <article uk-height-viewport="offset-top: true; offset-bottom: true">
        <?php
        echo "<h1 class='uk-text-center'>Dernieres Videos</h1>";
        
        DisplayLatestVideos();

        echo "<h1 class='uk-text-center'>Les plus vues</h1>";
        
        DisplayMostViewedVideos();

        ?>
    </article>

    <?php include_once("../php/footer.php"); ?>

</body>

</html>