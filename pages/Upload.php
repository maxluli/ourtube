<?php
session_start();
if (!isset($_SESSION["TotalUploaded"])) {
    $_SESSION["TotalUploaded"] = 0;
}
$message = "";
require("../php/fonctions.php");
if (!isset($_SESSION["username"])) {
    //if the user is not connected, he cant go into this page
    //header("location:login.php");
} else {
}
if (isset($_POST["SubmitVideo"])) {

    $Title = filter_input(INPUT_POST, "Title", FILTER_SANITIZE_STRING);
    $Description = filter_input(INPUT_POST, "Description", FILTER_SANITIZE_STRING);

    if ($Title != "" && $Description != "" && file_exists($_FILES["Video"]["tmp_name"])) {

        $_SESSION['TotalUploaded'] += (int)(filesize(($_FILES['Video']['tmp_name'])) / (1024 * 1024));
        if ($_SESSION['TotalUploaded'] > 1048) {
            //the user can upload to 1gb of data for one session
            $message .= "
                <div class='uk-alert-danger uk-border-rounded' uk-alert style='background-color:#d64550;color:black'>
                <a class='uk-alert-close' uk-close></a>
                <p>You uploaded too much data : {$_SESSION['TotalUploaded']} MB<br>
                So your IP/account will be blacklisted !</p>
                </div>";
        } else {
            $message .= addVideo($Title, $Description);
        }
    } else {
        //echo $Title . "</br>" . $Description;
        $message .= "
        <div class='uk-alert-danger uk-border-rounded' uk-alert style='background-color:#d64550;color:black'>
        <a class='uk-alert-close' uk-close></a>
        <p>Please fill all the infos</p>
        </div>";
    }
} else {
    $message .=
        "<div class='uk-alert-warning uk-border-rounded' uk-alert style='background-color:#ffbb33;color:black'>
    <a class='uk-alert-close' uk-close></a>
    <p>Site under Construction and heavy devloppement</p>
    </div>";
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="../css/uikit.min.css" />
    <script src="../js/uikit.min.js"></script>
    <script src="../js/uikit-icons.min.js"></script>

    <title>Upload</title>
</head>
<?php include_once("../php/nav.php"); ?>
<article class="uk-text-center uk-background-muted" uk-height-viewport="offset-top: true; offset-bottom: true">
    <div class="uk-text-center uk-align-center uk-width-xlarge uk-background-secondary uk-light">
        <div class="uk-margin">
            <h1 class="">Upload a video to OurTube</h1>
        </div>
        <form action="#" method="POST" enctype="multipart/form-data">

            <fieldset class="uk-fieldset">

                <div class="uk-margin">
                    <input class="uk-input uk-form-width-large" type="text" placeholder="Video Title" name="Title" required>
                </div>
                <div class="uk-margin">
                    <textarea class="uk-textarea uk-form-width-large" rows="5" placeholder="Video Description" name="Description" required></textarea>
                </div>
                <div class="uk-margin" uk-margin>
                    <div uk-form-custom="target: true">
                        <input type="file" name="Video" accept=".mp4" onchange="TheloadFile(event)" required>
                        <input class="uk-input uk-form-width-large uk-align-center" type="text" placeholder="Video Here" disabled>
                    </div>
                    <video id="theOutput" width="320" height="240" controls></video>
                </div>

                <script>
                //found on internet
                    var TheloadFile = function(event) {
                        var output = document.getElementById('theOutput');
                        output.src = URL.createObjectURL(event.target.files[0]);
                        output.onload = function() {
                            URL.revokeObjectURL(output.src) // free memory
                        }
                    };
                </script>

                <div>
                    <div uk-form-custom="target: true">
                        <input type="file" name="Thumbnail" accept=".png,.jpg" onchange="loadFile(event)">
                        <input class="uk-input uk-align-left" type="text" placeholder="Thumbnail here" disabled>
                    </div>
                    
                </div>

                <img id="output" width="320" height="240">
                
                <script>
                //found on internet
                    var loadFile = function(event) {
                        var output = document.getElementById('output');
                        output.src = URL.createObjectURL(event.target.files[0]);
                        output.onload = function() {
                            URL.revokeObjectURL(output.src) // free memory
                        }
                    };
                </script>

                <div class="uk-margin">
                    <button class="uk-button uk-button-default uk-form-width-large" name="SubmitVideo">Post</button>
                </div>
        </form>
        </fieldset>
        <div>
            <div class="uk-background-muted uk-margin-top" name="Error Messages" style="padding-top:5%">
                <?= $message ?>
            </div>
</article>

<?php include_once("../php/footer.php"); ?>

</body>

</html>