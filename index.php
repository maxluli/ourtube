<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="css/uikit.min.css" />
    <script src="js/uikit.min.js"></script>
    <script src="js/uikit-icons.min.js"></script>

    <title>Document</title>
</head>

<body>
    <h1>OurTube Index</h1>
    <p>You will be automatically redirected in a few seconds</p>
    <p>If not , click <a href="pages">here</a></p>
</body>

</html>

<?php
header("location:pages");