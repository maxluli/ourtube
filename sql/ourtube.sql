-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 27, 2021 at 08:36 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ourtube`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_adminlevels`
--

CREATE TABLE `t_adminlevels` (
  `idLevel` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_channels`
--

CREATE TABLE `t_channels` (
  `idChannel` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `logoLocation` varchar(80) NOT NULL,
  `baneerLocation` varchar(80) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_comments`
--

CREATE TABLE `t_comments` (
  `idCommentaire` int(11) NOT NULL,
  `idVideo` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `commentDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `likes` int(11) NOT NULL,
  `dislikes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_histo`
--

CREATE TABLE `t_histo` (
  `idUser` int(11) NOT NULL,
  `idVideo` int(11) NOT NULL,
  `dateView` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_playlist`
--

CREATE TABLE `t_playlist` (
  `idPlaylist` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_playlistcontents`
--

CREATE TABLE `t_playlistcontents` (
  `idPlaylist` int(11) NOT NULL,
  `idVideo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_reports`
--

CREATE TABLE `t_reports` (
  `idReport` int(11) NOT NULL,
  `idVideo` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `reason` text NOT NULL,
  `reportDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_subscribes`
--

CREATE TABLE `t_subscribes` (
  `idUser` int(11) NOT NULL,
  `idChaine` int(11) NOT NULL,
  `subDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_users`
--

CREATE TABLE `t_users` (
  `idUser` int(11) NOT NULL,
  `idLevel` int(11) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(80) NOT NULL,
  `JoinDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `banned` tinyint(1) NOT NULL,
  `ppLocation` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_videos`
--

CREATE TABLE `t_videos` (
  `idVideo` int(11) NOT NULL,
  `idChaine` int(11) NOT NULL,
  `vues` int(10) NOT NULL,
  `publishDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(80) NOT NULL,
  `public` tinyint(1) NOT NULL,
  `likes` int(10) NOT NULL,
  `dislikes` int(10) NOT NULL,
  `banned` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_videos`
--

INSERT INTO `t_videos` (`idVideo`, `idChaine`, `vues`, `publishDate`, `title`, `description`, `location`, `public`, `likes`, `dislikes`, `banned`) VALUES
(17, 1, 0, '2021-04-27 07:28:11', 'Castle in the sky', 'yaaaay', '../videos/6087bd0b29174.mp4', 1, 0, 0, 0),
(18, 1, 0, '2021-04-27 07:29:23', 'Castle in the fucking sky ', 'YAAAAAY', '../videos/6087bd533d8ac.mp4', 1, 0, 0, 0),
(19, 1, 0, '2021-04-27 07:31:01', 'YOU SEE THIS CASTLE', 'omg it flyes', '../videos/6087bdb5393cc.mp4', 1, 0, 0, 0),
(20, 1, 0, '2021-04-27 07:31:38', 'Oh nevermind its the chateau ambulant', 'it just.. walks', '../videos/6087bddab524b.mp4', 1, 0, 0, 0),
(21, 1, 0, '2021-04-27 08:24:47', 'YAY', 'YAAAAAAA', '../videos/6087ca4f55844.mp4', 1, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_adminlevels`
--
ALTER TABLE `t_adminlevels`
  ADD PRIMARY KEY (`idLevel`);

--
-- Indexes for table `t_channels`
--
ALTER TABLE `t_channels`
  ADD PRIMARY KEY (`idChannel`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `t_comments`
--
ALTER TABLE `t_comments`
  ADD PRIMARY KEY (`idCommentaire`),
  ADD KEY `idVideo` (`idVideo`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `t_histo`
--
ALTER TABLE `t_histo`
  ADD KEY `idUser` (`idUser`),
  ADD KEY `idVideo` (`idVideo`);

--
-- Indexes for table `t_playlist`
--
ALTER TABLE `t_playlist`
  ADD PRIMARY KEY (`idPlaylist`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `t_playlistcontents`
--
ALTER TABLE `t_playlistcontents`
  ADD KEY `idPlaylist` (`idPlaylist`),
  ADD KEY `idVideo` (`idVideo`);

--
-- Indexes for table `t_reports`
--
ALTER TABLE `t_reports`
  ADD PRIMARY KEY (`idReport`),
  ADD KEY `idVideo` (`idVideo`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `idLevel` (`idLevel`);

--
-- Indexes for table `t_videos`
--
ALTER TABLE `t_videos`
  ADD PRIMARY KEY (`idVideo`),
  ADD KEY `idChaine` (`idChaine`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_adminlevels`
--
ALTER TABLE `t_adminlevels`
  MODIFY `idLevel` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_channels`
--
ALTER TABLE `t_channels`
  MODIFY `idChannel` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_comments`
--
ALTER TABLE `t_comments`
  MODIFY `idCommentaire` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_playlist`
--
ALTER TABLE `t_playlist`
  MODIFY `idPlaylist` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_reports`
--
ALTER TABLE `t_reports`
  MODIFY `idReport` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_users`
--
ALTER TABLE `t_users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_videos`
--
ALTER TABLE `t_videos`
  MODIFY `idVideo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
